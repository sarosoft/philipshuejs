import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import { Component, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

import * as fromRoot from '../reducers';
import * as lights from '../actions/hue-lights';

@Component({
  selector: 'hue-lights-page',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
  	<md-card>
      <md-card-title>List of Lights</md-card-title>
    </md-card>
    <hue-light-list [lights]="lights$ | async"></hue-light-list>
  `, 
  styles: [`
    md-card-title {
      display: flex;
      justify-content: center;
    }
  `]
})
export class HueLightsPageComponent {
  lights$: Observable<any>;  

  constructor(store: Store<fromRoot.State>) {
    this.lights$ = store.select(fromRoot.hueGetLightsData);
  }

}
