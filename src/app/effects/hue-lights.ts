import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/takeUntil';
import { Injectable } from '@angular/core';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { empty } from 'rxjs/observable/empty';
import { of } from 'rxjs/observable/of';

import { HueService } from '../services/hue';
import * as hueLights from '../actions/hue-lights';

@Injectable()
export class HueLightsEffects {

  @Effect()
  hueGetLights$: Observable<Action> = this.actions$
    .ofType(hueLights.ActionTypes.LIST)
    .startWith(new hueLights.ListAction())
    .switchMap(() => 
      this.hueService.hueGetLights()
        .map((data) => new hueLights.LoadSuccessAction(data))   
    );

    constructor(private actions$: Actions, private hueService: HueService) { }
}
