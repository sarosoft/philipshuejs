import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { config } from '../config';

@Injectable()
export class HueService {
	private API_PATH = 
		'http://' + config.HueService.IpAddress + '/api/' + config.HueService.ApiKey;

  constructor(private http: Http) {}

  hueGetLights(): Observable<any> {
    return this.http.get(`${this.API_PATH}/lights`)
    	//.do(x => console.log(x.json()) )
      .map(res => res.json() || {});
  }

}
