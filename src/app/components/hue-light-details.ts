import { Component, Input, EventEmitter } from '@angular/core';
import { ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

import * as _ from 'lodash';

@Component({
  selector: 'hue-light-details', 
  template: `
<button type="button" class="btn btn-primary" (click)="showModal()">Details</button>
<div *ngIf="isModalShown" [config]="{ backdrop: false, show: true }" (onHidden)="onHidden()" bsModal #autoShownModal="bs-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title pull-left">{{ light.name }}</h4>
        <button type="button" class="close pull-right" aria-label="Close" (click)="hideModal()">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <div *ngFor="let key of lightKeys">
            <div [ngSwitch]="key" class="row">
              <div *ngSwitchCase="'state'">
                <div class="col-xs-3">state</div>
                <div class="col-xs-9">
                  {{ getItem(key+'.alert') }}
https://developers.meethue.com/documentation/color-conversions-rgb-xy
                </div>
              </div>
              <div *ngSwitchDefault>
                <div class="col-xs-3">{{ key }}</div>
                <div class="col-xs-9">{{ getItem(key) }}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> `
})

export class HueLightDetailsComponent {
  
  @Input() 
  public light:any;

  @ViewChild('autoShownModal') 
  public autoShownModal:ModalDirective;

  public isModalShown:boolean = false;
 
  private lightKeys:string[];

  constructor() { }

  public getItem(key:string):string {
    return _.get(this.light, key, '-');
  }

  public showModal():void {
    this.lightKeys = _.keys(this.light);
    this.isModalShown = true;
    console.log('lightKeys: ', this.lightKeys);

    console.log('light: ', this.light);
  }
 
  public hideModal():void {
    this.autoShownModal.hide();
  }
 
  public onHidden():void {
    this.isModalShown = false;
  }

}
