import { Component, Input, EventEmitter } from '@angular/core';
import { ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

import * as _ from 'lodash';

@Component({
  selector: 'hue-light-list',
  template: `
    <div class="container">
      <div class="row" *ngFor="let light of lightsArray">
        <div class="col-xs-4">{{ light.name }}</div>
        <div class="col-xs-2">{{ light.manufacturername }}</div>
        <div class="col-xs-4">{{ light.type }}</div>
        <div class="col-xs-2"><hue-light-details [light]="light"></hue-light-details></div>
      </div>
    </div>
  `,
  styles: [`
    :host {
      display: flex;
      flex-wrap: wrap;
      justify-content: center;
    }
  `]
})

export class HueLightListComponent {
  @Input() lights: any;

  constructor() {}

  get lightsArray() {
    return _.values(this.lights);
  }

}
