import * as hueLights from '../actions/hue-lights';

export interface State {
  loaded: boolean;
  loading: boolean;
  data: Object;
};

const initialState: State = {
  loaded: false,
  loading: false,
  data: null,
};

export function reducer(state = initialState, action: hueLights.Actions): State {

	switch (action.type) {

		case hueLights.ActionTypes.LIST: {
      return Object.assign({}, state, {
        loading: true
      });
		}

    case hueLights.ActionTypes.LOAD_SUCCESS: {
      return {
        loaded: true,
        loading: false,
        data: action.payload
      };
    }

    default: {
    	return state;
    }
	}
}

export const hueGetLights = (state: State) => state.loaded;

export const hueGetLightsData = (state: State) => state.data;
