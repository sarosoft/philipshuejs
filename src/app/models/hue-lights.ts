// TODO: Implement data models?
export interface HueLights {

}

{
  "3": {
    "state": {
      "on": false,
      "bri": 159,
      "hue": 9296,
      "sat": 215,
      "effect": "none",
      "xy": [
        0.5480,
        0.3907
      ],
      "ct": 500,
      "alert": "none",
      "colormode": "xy",
      "reachable": true
    },
    "type": "Extended color light",
    "name": "Essen III",
    "modelid": "LCT007",
    "manufacturername": "Philips",
    "uniqueid": "00:17:88:01:10:2a:7e:b4-0b",
    "swversion": "5.50.1.19085"
  },
  "4": {
    "state": {
      "on": false,
      "bri": 145,
      "hue": 7676,
      "sat": 199,
      "effect": "none",
      "xy": [
        0.5016,
        0.4151
      ],
      "ct": 443,
      "alert": "none",
      "colormode": "xy",
      "reachable": true
    },
    "type": "Extended color light",
    "name": "Bett Strip",
    "modelid": "LST002",
    "manufacturername": "Philips",
    "uniqueid": "00:17:88:01:01:1e:13:87-0b",
    "swversion": "5.50.2.19072"
  },
  "5": {
    "state": {
      "on": false,
      "bri": 254,
      "hue": 5021,
      "sat": 244,
      "effect": "none",
      "xy": [
        0.5810,
        0.3848
      ],
      "ct": 153,
      "alert": "none",
      "colormode": "hs",
      "reachable": true
    },
    "type": "Extended color light",
    "name": "Fenster links",
    "modelid": "LLC020",
    "manufacturername": "Philips",
    "uniqueid": "00:17:88:01:01:16:fa:68-0b",
    "swversion": "5.38.3.19141"
  },
  "6": {
    "state": {
      "on": false,
      "bri": 254,
      "hue": 5020,
      "sat": 244,
      "effect": "none",
      "xy": [
        0.5810,
        0.3848
      ],
      "ct": 153,
      "alert": "none",
      "colormode": "hs",
      "reachable": true
    },
    "type": "Extended color light",
    "name": "Fenster rechts",
    "modelid": "LLC020",
    "manufacturername": "Philips",
    "uniqueid": "00:17:88:01:01:15:ec:2b-0b",
    "swversion": "5.38.3.19141"
  },
  "7": {
    "state": {
      "on": false,
      "bri": 254,
      "hue": 7672,
      "sat": 199,
      "effect": "none",
      "xy": [
        0.5017,
        0.4151
      ],
      "ct": 443,
      "alert": "none",
      "colormode": "xy",
      "reachable": true
    },
    "type": "Extended color light",
    "name": "Küche Strip",
    "modelid": "LST002",
    "manufacturername": "Philips",
    "uniqueid": "00:17:88:01:01:1f:3e:8b-0b",
    "swversion": "5.50.2.19072"
  },
  "9": {
    "state": {
      "on": false,
      "bri": 145,
      "hue": 7676,
      "sat": 199,
      "effect": "none",
      "xy": [
        0.5016,
        0.4151
      ],
      "ct": 443,
      "alert": "none",
      "colormode": "xy",
      "reachable": true
    },
    "type": "Extended color light",
    "name": "Wohnzimmer Strip",
    "modelid": "LST002",
    "manufacturername": "Philips",
    "uniqueid": "00:17:88:01:01:1f:3b:92-0b",
    "swversion": "5.50.2.19072"
  },
  "11": {
    "state": {
      "on": false,
      "bri": 1,
      "alert": "none",
      "reachable": true
    },
    "type": "Dimmable light",
    "name": "Küche I",
    "modelid": "LWB010",
    "manufacturername": "Philips",
    "uniqueid": "00:17:88:01:02:6c:bd:80-0b",
    "swversion": "1.15.2_r19181",
    "swconfigid": "60083D2F",
    "productid": "Philips-LWB010-1-A19DLv3"
  },
  "12": {
    "state": {
      "on": false,
      "bri": 254,
      "hue": 5021,
      "sat": 244,
      "effect": "none",
      "xy": [
        0.5810,
        0.3848
      ],
      "ct": 153,
      "alert": "none",
      "colormode": "hs",
      "reachable": true
    },
    "type": "Extended color light",
    "name": "Küchenfenster",
    "modelid": "LLC020",
    "manufacturername": "Philips",
    "uniqueid": "00:17:88:01:01:17:62:ee-0b",
    "swversion": "5.38.3.19141"
  },
  "14": {
    "state": {
      "on": false,
      "bri": 159,
      "hue": 13524,
      "sat": 200,
      "effect": "none",
      "xy": [
        0.5017,
        0.4152
      ],
      "ct": 443,
      "alert": "none",
      "colormode": "xy",
      "reachable": true
    },
    "type": "Extended color light",
    "name": "Essen II",
    "modelid": "LCT007",
    "manufacturername": "Philips",
    "uniqueid": "00:17:88:01:10:2a:71:92-0b",
    "swversion": "5.50.1.19085"
  },
  "15": {
    "state": {
      "on": false,
      "bri": 230,
      "hue": 8841,
      "sat": 254,
      "effect": "none",
      "xy": [
        0.5831,
        0.3897
      ],
      "ct": 153,
      "alert": "none",
      "colormode": "hs",
      "reachable": true
    },
    "type": "Extended color light",
    "name": "Flur color",
    "modelid": "LCT007",
    "manufacturername": "Philips",
    "uniqueid": "00:17:88:01:10:2a:69:64-0b",
    "swversion": "5.50.1.19085"
  },
  "17": {
    "state": {
      "on": false,
      "bri": 1,
      "alert": "none",
      "reachable": true
    },
    "type": "Dimmable light",
    "name": "Küche II",
    "modelid": "LWB006",
    "manufacturername": "Philips",
    "uniqueid": "00:17:88:01:10:2f:9b:1a-0b",
    "swversion": "5.38.2.19136"
  },
  "19": {
    "state": {
      "on": false,
      "bri": 159,
      "hue": 7676,
      "sat": 199,
      "effect": "none",
      "xy": [
        0.5016,
        0.4151
      ],
      "ct": 443,
      "alert": "none",
      "colormode": "xy",
      "reachable": true
    },
    "type": "Extended color light",
    "name": "Essen I",
    "modelid": "LCT010",
    "manufacturername": "Philips",
    "uniqueid": "00:17:88:01:02:79:28:f4-0b",
    "swversion": "1.15.2_r19181",
    "swconfigid": "F921C859",
    "productid": "Philips-LCT010-1-A19ECLv4"
  },
  "20": {
    "state": {
      "on": false,
      "bri": 58,
      "ct": 450,
      "alert": "none",
      "colormode": "ct",
      "reachable": true
    },
    "type": "Color temperature light",
    "name": "Flur hinten 1",
    "modelid": "LTW001",
    "manufacturername": "Philips",
    "uniqueid": "00:17:88:01:10:2a:8b:44-0b",
    "swversion": "5.50.1.19085"
  },
  "21": {
    "state": {
      "on": false,
      "bri": 86,
      "ct": 450,
      "alert": "none",
      "colormode": "ct",
      "reachable": true
    },
    "type": "Color temperature light",
    "name": "Flur hinten 2",
    "modelid": "LTW001",
    "manufacturername": "Philips",
    "uniqueid": "00:17:88:01:10:2a:8e:75-0b",
    "swversion": "5.50.1.19085"
  }
}