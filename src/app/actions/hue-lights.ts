import { Action } from '@ngrx/store';
import { type } from '../util';

export const ActionTypes = {
  LIST:           	type('[Hue] List'),
  LOAD_SUCCESS:     type('[Hue] Load Success'),
};

export class ListAction implements Action {
  type = ActionTypes.LIST;

  constructor() { }
}

export class LoadSuccessAction implements Action {
  type = ActionTypes.LOAD_SUCCESS;

  constructor(public payload: any) { }
}

export type Actions
  = ListAction
  | LoadSuccessAction
