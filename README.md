# PhilipsHueJS

This application aims to manage a local Philips Hue system. The application is based on the [@ngrx example application](https://github.com/ngrx/example-app). 

I develop this application to learn Angular2, ngrx, etc.

The state of development is at a very early state. Please contact me if you like to contribute.

Some keywords to describe the technologies:
- [Angular2](https://v2.angular.io/docs/ts/latest/)
- [ngrx - Reactive Extensions for Angular](https://github.com/ngrx)
- [TypeScript](https://www.typescriptlang.org/docs/tutorial.html)
- [ngx-bootstrap](http://valor-software.com/ngx-bootstrap/#/)
- ...

## Quick start

Clone the repo
```bash
git clone https://bitbucket.org/sarosoft/philipshuejs.git
git checkout develop
```

Change directory to repo
```bash
cd PhilipsHueJS
```

Use npm to install the dependencies:
```bash
npm install
```

Create your configuration:
```bash
touch src/app/config.ts
```

Save your configuration:
```typescript
export const config = {
	HueService: {
		IpAddress: 'your-ip-address', 
		ApiKey: 'your-api-key',
	}
}
```
See: https://developers.meethue.com/documentation/getting-started

# start the server
```bash
npm start
```

Navigate to [http://localhost:4200/](http://localhost:4200/) in your browser.
